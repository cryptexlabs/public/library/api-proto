import {dirname} from "path";

export class Path {
    public static getProtoPath(): string {
        return dirname(dirname(__dirname)) + "/definitions";
    }
}