"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("path");
class Path {
    static getProtoPath() {
        return path_1.dirname(path_1.dirname(__dirname)) + "/definitions";
    }
}
exports.Path = Path;
//# sourceMappingURL=path.js.map